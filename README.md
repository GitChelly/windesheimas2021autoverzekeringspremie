```
  _____          _____   _____ _    _ _____  ______ 
 / ____|   /\   |  __ \ / ____| |  | |  __ \|  ____|
| |       /  \  | |__) | (___ | |  | | |__) | |__   
| |      / /\ \ |  _  / \___ \| |  | |  _  /|  __|  
| |____ / ____ \| | \ \ ____) | |__| | | \ \| |____ 
 \_____/_/    \_\_|  \_\_____/ \____/|_|  \_\______|
```
 
## Hier worden de unittesten die voor deze applicatie gemaakt zijn beschreven en toegelicht.
- Het doel van de unittest
- Waarom voor deze data van de unittest is gekozen
- Welke technieken gebruikt zijn om tot deze data te komen

<br/>

## DE UNIT TESTS
- BerekeningBasisPremieTest()
- OpslagJonger23Test()
- OpslagMinder5jaarTest()
- opslagPostcode5procent()
- opslagPostcode2procent()
- WAPlusVsWATest()
- AllRiskVsWATest()
- SchadevrijeJarenTest()
- maandPremie()
- jaarPremie()

<br/> 

#### BerekeningBasisPremieTest()
- Hier is getest of de basis premie correct is berekend.
- Voor de data heb ik deels mijn eigen auto gebruikt.
- Er is gebruik gemaakt van een fact.

<br/>

#### OpslagJonger23Test()
- Hier is getest of er daadwerkelijk een 15% toeslag voor de jonge bestuurder ( jonger dan 23 jaar ) word gerekend vergeleken met een oude bestuurder.
- Voor de data heb ik gebruik gemaakt van een persoon jonger dan 23 (18), en een persoon ouder dan 23 (55), zodat er een prijs verschil is.
- Er is gebruik gemaakt van een fact.

<br/>

#### OpslagMinder5jaarTest()
- Hier is getest of er daadwerkelijk een 15% toeslag voor een bestuurder met minder dan 5 jaar rijbewijs word gerekend vergeleken met een bestuurder die 5 jaar of langer zijn rijbewijs heeft.
- Voor de data heb ik gebruik gemaakt van de variabele "startDatumRijbewijsJongeBestuurder" omdat de tijd verandert. Dmv deze variabele word de tijd als het ware ge-update.
- Er is gebruik gemaakt van een Theory.

<br/>

#### opslagPostcode5procent()
- Hier is getest of er daadwerkelijk een 5% toeslag voor de postcodes (10xx - 35xx) worden gerekend vergeleken met postcodes boven de 4500 zonder toeslag.
- Voor de data heb ik gebruik gemaakt van de postcodes 2222 en 8800, omdat ze binnen de 5% toeslag waardes (10xx - 35xx) en 0% toeslag waardes vallen.
- Er is gebruik gemaakt van een fact.

<br/>

#### opslagPostcode2procent()
- Hier is getest of er daadwerkelijk een 2% toeslag voor de postcodes (36xx - 44xx) worden gerekend vergeleken met postcodes boven de 4500 zonder toeslag.
- Voor de data heb ik gebruik gemaakt van de postcodes 3800 en 8800, omdat ze binnen de 2% toeslag waardes (36xx - 44xx) en 0% toeslag waardes vallen.
- Er is gebruik gemaakt van een fact.

<br/>

#### WAPlusVsWATest()
- Hier is getest of er daadwerkelijk een 20% toeslag voor de WAPlus Verzekering wordt gerekend vergeleken met de WA verzekering.
- Voor de data heb ik gebruik gemaakt van de InsuranceCoverage.WAPlus en InsuranceCoverage.WA. Hierbij zijn WAplus en WA * 1.20 met elkaar vergeleken vanwege de 20% toeslag
- Er is gebruik gemaakt van een fact.

<br/>

#### AllRiskVsWATest()
- Hier is getest of de Allrisk verzekering daadwerkelijk 2x zoveel kost als de WA verzekering.
- Voor de data heb ik gebruik gemaakt van de InsuranceCoverage.ALL_RISK en InsuranceCoverage.WA. Hierbij zijn Allrisk en WA * 2 met elkaar vergeleken.
- Er is gebruik gemaakt van een fact.

<br/>

#### SchadevrijeJarenTest()
- Hier is getest of er daadwerkelijk 5% korting voor elk schadevrije jaar ( vanaf 6 jaar ) wordt gegeven.
Voor de data heb ik gebruik gemaakt van de variabele "OngeclaimdeJaren" omdat verschillende aantal schadevrije jaren gecheckt moesten worden. 
- Er is gebruik gemaakt van een Theory.

<br/>

#### maandPremie()
- Hier is getest of de maandpremie goed wordt berekend. 
- Voor deze data zijn de verwachtte berekende maandpremie en de berekende maandpremie door de PremiumCalculation() vergeleken met elkaar.
- Er is gebruik gemaakt van een fact.

<br/>

#### jaarPremie()
- Hier is getest of de jaarpremie goed wordt berekend. 
- Voor deze data zijn de verwachtte berekende jaarpremie en de berekende jaarpremie door de PremiumCalculation() vergeleken met elkaar.
- Er is gebruik gemaakt van een fact.

<br/>


## GEVONDEN EN AANGEPASTE CODE
```

 if(policyHolder.Age < 23 || policyHolder.LicenseAge <= 5)
            {
                premium *= 1.15;
            }

```
veranderd naar : 

```
 if(policyHolder.Age < 23 || policyHolder.LicenseAge < 5)
            {
                premium *= 1.15;
            }
```
omdat er het volgende in de requirements staat: " korter dan 5j rijbewijs is premie-opslag van 15% ",  " < = " is gelijk aan 5 of minder dan 5 en dat klopt niet.




```
 private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            int NoClaimPercentage = (years - 5) * 5;
            if (NoClaimPercentage > 65) { NoClaimPercentage = 65; }
            if (NoClaimPercentage < 0) { NoClaimPercentage = 0; }
            return premium * ((100 - NoClaimPercentage) / 100);
        }
```

veranderd naar : 

```
private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            int NoClaimPercentage = (years - 5) * 5;
            if (NoClaimPercentage > 65) { NoClaimPercentage = 65; }
            if (NoClaimPercentage < 0) { NoClaimPercentage = 0; }
            return premium * ((double)(100 - NoClaimPercentage) / 100);
        }
```
Een double toegevoegd, omdat het afronden dan niet overeenkomt met de vergelijkende waarde in de test file.






```
  internal double PremiumPaymentAmount(PaymentPeriod period)
        {
            double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear / 1.025 : PremiumAmountPerYear / 12;
            return Math.Round(result, PRECISION);
        }
```

veranderd naar : 

```
  internal double PremiumPaymentAmount(PaymentPeriod period)
        {
            double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear * (1 - 0.025) : PremiumAmountPerYear / 12;
            return Math.Round(result, PRECISION);
        }
```






```
internal static double CalculateBasePremium(Vehicle vehicle)
        {
            return vehicle.ValueInEuros / 100 - vehicle.Age + vehicle.PowerInKw / 5 / 3;
        }
```

veranderd naar:

```
internal static double CalculateBasePremium(Vehicle vehicle)
        {
            return ((double)vehicle.ValueInEuros / 100 - vehicle.Age + (double)vehicle.PowerInKw / 5) / 3;
        }
```

Doubles toegevoegd, omdat het afronden dan niet overeenkomt met de vergelijkende waarde in de test file.

