using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using Moq;

namespace AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTest
    {
        private Mock<Vehicle>MockVehicle(int age = 2012, int valueInEuros = 7000, int powerInKw = 200)
        {
            var mockVehicle = new Mock<Vehicle>(MockBehavior.Default, 0, 0, 0);
            mockVehicle.Setup(Vehicle => Vehicle.Age).Returns(age);
            mockVehicle.Setup(Vehicle => Vehicle.ValueInEuros).Returns(valueInEuros);
            mockVehicle.Setup(Vehicle => Vehicle.PowerInKw).Returns(powerInKw);
            return mockVehicle;
        }

        [Fact]
        public void BerekeningBasisPremieTest()
        {
            // Arrange
            var vehicle = new Vehicle(200, 7000, 2012);

            var expectedPremium = ((double)vehicle.ValueInEuros / 100 - vehicle.Age + (double)vehicle.PowerInKw / 5) / 3;

            //Act
            var actualValue = PremiumCalculation.CalculateBasePremium(vehicle);

            //Assert
            Assert.Equal(expectedPremium, actualValue);
        }

        [Fact]
        public void OpslagJonger23Test()
        {
            //Arrange
            var mockVehicle = MockVehicle();

            var jongeBestuurder = new PolicyHolder(18, "01-01-2005", 5005, 0);
            var oudeBestuurder = new PolicyHolder(55, "01-01-2005", 5005, 0);

            //Act
            var premieOudBestuurder = new PremiumCalculation(mockVehicle.Object, oudeBestuurder, InsuranceCoverage.WA);

            var premieJongeBestuurder = new PremiumCalculation(mockVehicle.Object, jongeBestuurder, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premieJongeBestuurder.PremiumAmountPerYear, premieOudBestuurder.PremiumAmountPerYear * 1.15);
        }

        [Theory]
        [InlineData(5, false)]
        [InlineData(4, true)]
        public void OpslagMinder5jaarTest(int rijbewijsLeeftijd, bool verhoging)
        {
            //Arrange
            var mockVehicle = MockVehicle();

            var startDatumRijbewijsJongeBestuurder = DateTime.Now.AddYears(-rijbewijsLeeftijd).ToString();
            var jongeBestuurder = new PolicyHolder(33, startDatumRijbewijsJongeBestuurder, 5005, 0);

            var oudeBestuurder = new PolicyHolder(33, "01-01-2005", 5005, 0);
            var premieOudBestuurder = new PremiumCalculation(mockVehicle.Object, oudeBestuurder, InsuranceCoverage.WA);

            var verwachttePremieWaarde = verhoging
                ? premieOudBestuurder.PremiumAmountPerYear * 1.15
                : premieOudBestuurder.PremiumAmountPerYear;

            //Act
            var premieJongeBestuurder = new PremiumCalculation(mockVehicle.Object, jongeBestuurder, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(verwachttePremieWaarde, premieJongeBestuurder.PremiumAmountPerYear);
        }

        [Fact]
        public void opslagPostcode5procent()
        {
            //Arrange
            var mockVehicle = MockVehicle();

            var postcode5Procent = new PolicyHolder(33, "01-01-2005", 2222, 0);
            var postcode0Procent = new PolicyHolder(33, "01-01-2005", 8800, 0);

            //Act
            var premie5Postcode = new PremiumCalculation(mockVehicle.Object, postcode5Procent, InsuranceCoverage.WA);
            var premie0Postcode = new PremiumCalculation(mockVehicle.Object, postcode0Procent, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premie5Postcode.PremiumAmountPerYear, premie0Postcode.PremiumAmountPerYear * 1.05);
        }

        [Fact]
        public void opslagPostcode2procent()
        {
            //Arrange
            var mockVehicle = MockVehicle();

            var postcode2Procent = new PolicyHolder(33, "01-01-2005", 3800, 0);
            var postcode0Procent = new PolicyHolder(33, "01-01-2005", 8800, 0);

            //Act
            var premie2Postcode = new PremiumCalculation(mockVehicle.Object, postcode2Procent, InsuranceCoverage.WA);
            var premie0Postcode = new PremiumCalculation(mockVehicle.Object, postcode0Procent, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premie2Postcode.PremiumAmountPerYear, premie0Postcode.PremiumAmountPerYear * 1.02);
        }

        [Fact]
        public void WAPlusVsWATest()
        {
            //Arrange 
            var mockVehicle = MockVehicle();

            var bestuurder = new PolicyHolder(33, "01-01-2005", 8800, 0);

            //Act
            var KeuzeWAplus = new PremiumCalculation(mockVehicle.Object, bestuurder, InsuranceCoverage.WA_PLUS);
            var KeuzeWA = new PremiumCalculation(mockVehicle.Object, bestuurder, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(KeuzeWAplus.PremiumAmountPerYear, KeuzeWA.PremiumAmountPerYear * 1.20);
        }

        [Fact]
        public void AllRiskVsWATest()
        {
            //Arrange 
            var mockVehicle = MockVehicle();

            var bestuurder = new PolicyHolder(33, "01-01-2005", 8800, 0);

            //Act
            var KeuzeAllRisk = new PremiumCalculation(mockVehicle.Object, bestuurder, InsuranceCoverage.ALL_RISK);
            var KeuzeWA = new PremiumCalculation(mockVehicle.Object, bestuurder, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(KeuzeAllRisk.PremiumAmountPerYear, KeuzeWA.PremiumAmountPerYear * 2);
        }

        [Theory]
        [InlineData(5, 0)]
        [InlineData(6, 5)]
        [InlineData(7, 10)]
        [InlineData(10, 25)]
        [InlineData(25, 65)]
        public void SchadevrijeJarenTest(int OngeclaimdeJaren, double Korting)
        {
            //Arrange 
            var mockVehicle = MockVehicle();

            var bestuurder1 = new PolicyHolder(33, "01-01-2005", 8800, 0);
            var bestuurder2 = new PolicyHolder(33, "01-01-2005", 8800, OngeclaimdeJaren);

            //Act
            var nietSchadeVrij = new PremiumCalculation(mockVehicle.Object, bestuurder1, InsuranceCoverage.WA);
            var SchadeVrij = new PremiumCalculation(mockVehicle.Object, bestuurder2, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(SchadeVrij.PremiumAmountPerYear, nietSchadeVrij.PremiumAmountPerYear * (1 - Korting / 100));
        }

        [Fact]
        public void maandPremie()
        {
            //Arrange
            var mockVehicle = MockVehicle();

            var bestuurder = new PolicyHolder(33, "01-01-2005", 8800, 0);

            var premieBerekening = new PremiumCalculation(mockVehicle.Object, bestuurder, InsuranceCoverage.WA);

            var verwachting = Math.Round(premieBerekening.PremiumAmountPerYear / 12, 2);

            //Act
            var waarde = premieBerekening.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            //Assert
            Assert.Equal(verwachting, waarde);

        }

        [Fact]
        public void jaarPremie()
        {
            //Arrange
            var mockVehicle = MockVehicle();

            var bestuurder = new PolicyHolder(33, "01-01-2005", 8800, 0);

            var premieBerekening = new PremiumCalculation(mockVehicle.Object, bestuurder, InsuranceCoverage.WA);

            var verwachting = Math.Round(premieBerekening.PremiumAmountPerYear * (1 - 0.025), 2);

            //Act
            var waarde = premieBerekening.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            //Assert
            Assert.Equal(verwachting, waarde);
        }



    }
}
