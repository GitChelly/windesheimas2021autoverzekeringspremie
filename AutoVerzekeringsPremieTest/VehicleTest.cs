﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace AutoVerzekeringsPremieTest
{
    public class VehicleTest
    {
        [Theory]
        [InlineData(2021, 0)]
        [InlineData(2019, 2)]
        [InlineData(2023, 0)]
        public void BerekeningAutoLeeftijd(int constructieJaar, int verwachtteAutoLeeftijd)
        {
            //Arrange
            var vehicle = new Vehicle(200, 7000, constructieJaar);

            //Act
            int huidigeAutoLeeftijd = vehicle.Age;

            //Assert
            Assert.Equal(verwachtteAutoLeeftijd, huidigeAutoLeeftijd);
        }
    }
}
